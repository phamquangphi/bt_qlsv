function renderDSSV(dssv) {
    //Hàm chạy render dssv tách hàm
    var innerHTML = "";
 for (var i =0 ; i< dssv.length; i++) {
   var sv = dssv[i];
   var content= `<tr>
   <td>${sv.ma}</td>
   <td> ${sv.ten} </td>
   <td> ${sv.email} </td>
   <td> ${sv.DTB()} </td>
   <td>
   <button class="btn btn-danger" onclick="suaThongTinSV('${sv.ma}')" >Sửa</button>
   <button class="btn btn-success" onclick="xoaSinhVien('${sv.ma}')">Xóa</button>
   </td>
   </tr>`;
    innerHTML += content;
 }
 document.getElementById("tbodySinhVien").innerHTML= innerHTML;
}
//gọi hàm xóa sinh viên
function xoaSinhVien(id) {
    var delet = dssv.findIndex(function (item) {
      return item.ma == id;
    });
    dssv.splice(delet, 1);
    renderDSSV(dssv);  
}
//gọi hàm sửa thông tin sinh viên
function suaThongTinSV(id){
  var index = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  //DOM tới hàm show thông tin sinh viên để base lên playout
  showThongTinSVLenForm(dssv[index]);
}
// nút cập nhật thông tin từ form
function layThongTinSV() {
  var ma =  document.getElementById("txtMaSV").value;
  var ten =   document.getElementById("txtTenSV").value;
  var email =  document.getElementById("txtEmail").value;
  var matkhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value*1;
  var ly = document.getElementById("txtDiemLy").value*1;
  var hoa = document.getElementById("txtDiemHoa").value*1;
  return new SinhVien(ma, ten, email, matkhau, toan, ly, hoa);
}