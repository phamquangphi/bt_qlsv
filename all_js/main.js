//khởi tạo dãy để truyền object
var dssv= [];
//Lấy dữ liệu từ local lúc user load trang
var dataJon = localStorage.getItem("DSSV");
if(dataJon != null){
    // dùng map để cung cấp kết quả từ hàm local đc dom lên playout
    dssv = JSON.parse(dataJon).map(function(item){
        return new SinhVien(
            item.ma,
            item.ten,
            item.email,
            item.matkhau,
            item.toan,
            item.ly,
            item.hoa,
        );
    })
    renderDSSV(dssv);
}
// Khởi tạo Hàm để user nhập giá trị
function themSinhVien() {
// Lấy thông tin sinh viên từ hàm laythongtinSV
var sv = layThongTinSV();
//push object vào dãy hàm
dssv.push(sv);
renderDSSV(dssv);
 // lưu danh sách sinh viên vào local
 var dataJon = JSON.stringify(dssv);
 localStorage.setItem("DSSV", dataJon);
}
// gọi một hàm show thông tin sinh viên lên form khi hover vào nút sửa
function showThongTinSVLenForm(sv) {
      document.getElementById("txtMaSV").value = sv.ma;
     document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
   document.getElementById("txtPass").value = sv.matkhau;
   document.getElementById("txtDiemToan").value = sv.toan;
   document.getElementById("txtDiemLy").value = sv.ly;
   document.getElementById("txtDiemHoa").value = sv.hoa;
}
//DOM đến click của 1 hàm
document.querySelector("#themSinhVien").addEventListener("click", themSinhVien);